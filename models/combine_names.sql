{{ config(materialized='table')}}

with supported_combine_names as (
    select 
        concat(name,name) AS concat_name from {{source('dbt_source','prefixsupported_sources')}}
)

select *
from supported_combine_names